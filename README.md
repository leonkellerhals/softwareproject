# Softwareproject BA #

## Theme: Web-application to generate a sudoku field and solve it ##

### Description: ###
* The Project is available at http://baproject.ba.he-hosting.de/
* Start a new game and choose your difficulty.
* Complete the Sudoku and check your result.
* If you want to continue your last game you have to register.
* Log in to save your highscore. (best time solving a game)
* You can play Sudoku with your mobile device, too.

### Features: ###
* PHP-side sudoku generator
* frontend to "play" the game
* Randomly generated Sudokus in three difficulties
* Highscore (with login)
* Ability to continue the last game (with login)

### How to run the project: ###
* For further information see: http://symfony.com/
* copy the project into a folder in your webserver-root
* reconfigure your \<project_root>/app/config/parameters.yml for your environment
* run the following commands under your \<project_home>
    1. php app/console doctrine:schema:update --force 
    2. php app/console assetic:dump
* now visit the file \<project_root>/web/app.php with a browser
* If you want to clear the cache or your server: php app/console cache:clear --env=prod --no-debug

### Used third party libraries: ###
* [jQuery](http://jquery.com/)
* [Bootstrap](http://getbootstrap.com/)
* [Symfony](http://symfony.com/)
* [phpass for Symfony](https://github.com/hautelook/phpass)
* [FriendsOfSymfony UserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle)

### Licence ###
* This project is licensed under the MIT-license (http://opensource.org/licenses/MIT)