<?php

namespace BAProject\SudokuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;


    public function __construct() {
        parent::__construct();
        $this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $games;


    /**
     * Add games
     *
     * @param \BAProject\SudokuBundle\Entity\Game $games
     * @return User
     */
    public function addGame(\BAProject\SudokuBundle\Entity\Game $games)
    {
        $this->games[] = $games;
    
        return $this;
    }

    /**
     * Remove games
     *
     * @param \BAProject\SudokuBundle\Entity\Game $games
     */
    public function removeGame(\BAProject\SudokuBundle\Entity\Game $games)
    {
        $this->games->removeElement($games);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGames()
    {
        return $this->games;
    }

}
