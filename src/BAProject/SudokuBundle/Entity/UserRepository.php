<?php

namespace BAProject\SudokuBundle\Entity;

use BAProject\SudokuBundle\Exception\InvalidGameRecordException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{

}
