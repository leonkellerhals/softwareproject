<?php

namespace BAProject\SudokuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 */
class Game
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $gamedata;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gamedata
     *
     * @param string $gamedata
     * @return Game
     */
    public function setGamedata($gamedata)
    {
        $this->gamedata = $gamedata;
    
        return $this;
    }

    /**
     * Get gamedata
     *
     * @return string
     */
    public function getGamedata()
    {
        return $this->gamedata;
    }
    /**
     * @var \BAProject\SudokuBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \BAProject\SudokuBundle\Entity\User $user
     * @return Game
     */
    public function setUser(\BAProject\SudokuBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \BAProject\SudokuBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var boolean
     */
    private $lastgame;

    /**
     * @var \DateTime
     */
    private $gamestart;

    /**
     * @var \DateTime
     */
    private $gameend;


    /**
     * Set lastgame
     *
     * @param boolean $lastgame
     * @return Game
     */
    public function setLastgame($lastgame)
    {
        $this->lastgame = $lastgame;
    
        return $this;
    }

    /**
     * Get lastgame
     *
     * @return boolean 
     */
    public function getLastgame()
    {
        return $this->lastgame;
    }

    /**
     * Set gamestart
     *
     * @param \DateTime $gamestart
     * @return Game
     */
    public function setGamestart($gamestart)
    {
        $this->gamestart = $gamestart;
    
        return $this;
    }

    /**
     * Get gamestart
     *
     * @return \DateTime 
     */
    public function getGamestart()
    {
        return $this->gamestart;
    }

    /**
     * Set gameend
     *
     * @param \DateTime $gameend
     * @return Game
     */
    public function setGameend($gameend)
    {
        $this->gameend = $gameend;
    
        return $this;
    }

    /**
     * Get gameend
     *
     * @return \DateTime 
     */
    public function getGameend()
    {
        return $this->gameend;
    }
    /**
     * @var boolean
     */
    private $loaded;


    /**
     * Set loaded
     *
     * @param boolean $loaded
     * @return Game
     */
    public function setLoaded($loaded)
    {
        $this->loaded = $loaded;

        return $this;
    }

    /**
     * Get loaded
     *
     * @return boolean 
     */
    public function getLoaded()
    {
        return $this->loaded;
    }
    /**
     * @var boolean
     */
    private $scored;


    /**
     * Set scored
     *
     * @param boolean $scored
     * @return Game
     */
    public function setScored($scored)
    {
        $this->scored = $scored;

        return $this;
    }

    /**
     * Get scored
     *
     * @return boolean 
     */
    public function getScored()
    {
        return $this->scored;
    }
    /**
     * @var integer
     */
    private $diff;


    /**
     * Set diff
     *
     * @param integer $diff
     * @return Game
     */
    public function setDiff($diff)
    {
        $this->diff = $diff;

        return $this;
    }

    /**
     * Get diff
     *
     * @return integer 
     */
    public function getDiff()
    {
        return $this->diff;
    }
}
