<?php

namespace BAProject\SudokuBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BAProjectSudokuBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
