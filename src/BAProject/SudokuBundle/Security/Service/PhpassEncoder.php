<?php

namespace BAProject\SudokuBundle\Security\Service;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Hautelook\Phpass\PasswordHash;

/**
 * A password encoder that uses Phpass for encoding
 *
 * @author Baldur Rensch <brensch@gmail.com>
 */
class PhpassEncoder implements PasswordEncoderInterface
{
    private static $iHashRounds = 10;

    public function encodePassword($raw, $salt)
    {
        $hasher = new PasswordHash(self::$iHashRounds, false);
        $hashedPassword = $hasher->hashPassword($raw);

        return $hashedPassword;
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        $hasher = new PasswordHash(self::$iHashRounds, false);

        return $hasher->CheckPassword($raw, $encoded);
    }
}