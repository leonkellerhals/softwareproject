<?php
namespace BAProject\SudokuBundle\Sudoku\Exception;

class InvalidInsertionIntoOriginal extends \RuntimeException {

	private $_row;
	private $_cell;

	public function __construct($row = null, $cell = null) {
		$this->_row = $row;
		$this->_cell = $cell;
	}

	public function getRow() {
		return $this->_row;
	}

	public function getCell() {
		return $this->_cell;
	}

}