<?php
namespace BAProject\SudokuBundle\Sudoku;

class Sudoku {

	/**
	 * numbers of given fields for the chosen difficulty
	 */
	const DIFFICULTY_EASY 	= 40;
	const DIFFICULTY_MEDIUM	= 31;
	const DIFFICULTY_HARD 	= 22;

	/**
	 * The original sudoku field without user's insertions. 
	 * @var SudokuField
	 */
	private $_original;

	/**
	 * The sudoku field with the user's insertions.
	 * @var SudokuField
	 */
	private $_inserted;

	/**
	 * The complete sudoku field without numbers removed.
	 * @var SudokuField
	 */
	private $_complete;

	private function __construct() {}

	public static function newGame($difficulty=self::DIFFICULTY_MEDIUM) {
		$sudoku = new Sudoku();

		$sudoku->_complete = SudokuGenerator::generate();
		$sudoku->_original = SudokuField::copy($sudoku->_complete);
		SudokuGenerator::pluck($sudoku->_original, $difficulty);
		$sudoku->_inserted = SudokuField::copy($sudoku->_original, false);

		return $sudoku;
	}

	public function getOriginal() {
		return $this->_original->getField();
	}

	public function getInserted() {
		return $this->_inserted->getField();
	}

	public function getComplete() {
		return $this->_complete->getField();
	}

	/**
	 * Insert a single number into the sudoku field. The number won't be checked. 
	 */
	public function insert($number, $row, $column) {
		if ($this->_original->getCell($row, $column) != 0) {
			throw new InvalidInsertionIntoOriginal();
		}
		$this->_inserted->insert($number, $row, $column);
	}

	/** 
	 * Checks the difference between the complete sudoku field and the inserted sudoku field.
	 * @return array a two-dimensional array containing 'true' for every field that was filled correctly, 'false' otherwise. 
	 */
	public function check() {
		$allCorrect = true;
		$diff = array();
		for ($i = 0; $i < 9; $i++) {
			for ($j = 0; $j < 9; $j++) {
				$diff[$i][$j] = $this->_complete->getCell($i, $j) == $this->_inserted->getCell($i, $j);
				if (!$diff[$i][$j]) $allCorrect = false;
			}
		}
		return $allCorrect ? $allCorrect : $diff;
	}

	public function getJSON() {
		return json_encode(array(
			'original' => $this->_original->getField(), 
			'complete' => $this->_complete->getField(),
			'inserted' => $this->_inserted->getField()
		));
	}

	public static function loadJSON($json) {
		$sudoku = new Sudoku();
		$decoded = json_decode($json);

		$sudoku->_original = new SudokuField($decoded->original);
		$sudoku->_complete = new SudokuField($decoded->complete);
		$sudoku->_inserted = new SudokuField($decoded->inserted, false);
		
		return $sudoku;
	}

}
