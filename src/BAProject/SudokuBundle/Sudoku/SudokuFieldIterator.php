<?php
namespace BAProject\SudokuBundle\Sudoku;

class SudokuFieldIterator {

	const ALL_FIELDS = 0;
	const EMPTY_FIELDS = 1;

	private $_sudokuField;
	private $_iteratorType;
	private $_currentRow;
	private $_currentColumn;
	
	public function __construct($sudokuField, $iteratorType) {
		$this->_sudokuField = $sudokuField;
		$this->_iteratorType = $iteratorType;
		$this->_currentRow = null;
		$this->_currentColumn = null;
		$this->_iteration = 0;
	}

	public function next() {
		// initialisation case: next() is called the first time. 
		if ($this->_currentRow === null && $this->_currentColumn === null) {
			$this->_currentRow = 0;
			$this->_currentColumn = 0;
			// if only empty fields shall be sent, recursively call this function until an empty field is found. 
			if ($this->_iteratorType == self::EMPTY_FIELDS && $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn) !== 0) {
				return $this->next();
			}
			return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
		}

		// standard case: inside a row.  
		if ($this->_currentColumn < 8) {
			$this->_currentColumn++;
			// if only empty fields shall be sent, recursively call this function until an empty field is found. 
			if ($this->_iteratorType == self::EMPTY_FIELDS && $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn) !== 0) {
				return $this->next();
			}
			return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
		}
		
		// end of sudokufield. 
		elseif ($this->_currentRow >= 8) {
			return false;
		}

		// end of row, start the next row. 
		$this->_currentRow++;
		$this->_currentColumn = 0;
		// if only empty fields shall be sent, recursively call this function until an empty field is found. 
		if ($this->_iteratorType == self::EMPTY_FIELDS && $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn) !== 0) {
			return $this->next();
		}
		return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
	}

	public function previous() {
		// initialisation case: next() wasn't called once yet.
		if ($this->_currentRow === null && $this->_currentColumn === null) {
			return false;
		}

		// standard case: inside a row.  
		if ($this->_currentColumn > 0) {
			$this->_currentColumn--;
			// if only empty fields shall be sent, recursively call this function until an empty field is found. 
			if ($this->_iteratorType == self::EMPTY_FIELDS && $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn) !== 0) {
				return $this->previous();
			}
			return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
		}
		
		// beginning of sudokufield. 
		elseif ($this->_currentRow <= 0) {
			return false;
		}

		// beginning of row, start the previous row. 
		$this->_currentRow--;
		$this->_currentColumn = 8;
		// if only empty fields shall be sent, recursively call this function until an empty field is found. 
		if ($this->_iteratorType == self::EMPTY_FIELDS && $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn) !== 0) {
			return $this->previous();
		}
		return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
	}

	public function reset() {
		$this->_currentColumn = null;
		$this->_currentRow = null;
	}

	public function getElement() {
		if ($this->_currentRow === null && $this->_currentColumn === null) {
			return null;
		}
		return $this->_sudokuField->getCell($this->_currentRow, $this->_currentColumn);
	}

	public function getRow() {
		return $this->_currentRow;
	}

	public function getColumn() {
		return $this->_currentColumn;
	}

}