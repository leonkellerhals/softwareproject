<?php
namespace BAProject\SudokuBundle\Sudoku;

use BAProject\SudokuBundle\Sudoku\Exception\InvalidInsertionException;
/**
 * Implements a sudoku field with any given segment size. 
 */
class SudokuField {

	/**
	 * A two dimensional array describing the sudoku field. 
	 * @var array
	 */
	private $_field;
	private $_check;

	public static function copy($original, $check = null) {
		if ($check === null) {
			return new SudokuField($original->_field, $original->_check);
		}
		return new SudokuField($original->_field, $check);
	}

	/**
	 * The constructor of the sudoku field class.
	 */
	public function __construct($field = null, $check = true) {
		$this->_check = $check;
		$this->_field = array(
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0),
			array(0, 0, 0,  0, 0, 0,  0, 0, 0)	
		);
		if (!$this->_check) {
			$this->_field = $field;
		} elseif ($field !== null) {
			for ($i = 0; $i < 9; $i++) {
				for ($j = 0; $j < 9; $j++) {
					if (!$this->insert($field[$i][$j], $i, $j)) {
						throw new InvalidInsertionException();
					}
				}
			}	
		}
	}

	/**
	 * Inserts a given number into a given cell. Throws InvalidInsertException if the number doesn't fit. 
	 * @param  int $number
	 * @param  int $row
	 * @param  int $column
	 * @return true if insertion was successful, false otherwise. 
	 */
	public function insert($number, $row, $column) {
		if (!$this->_check) {
			$this->_field[$row][$column] = $number;
			return true;	
		}
		if (!$this->checkInsert($number, $row, $column)) {
			return false;
		}
		$this->_field[$row][$column] = $number;
		return true;
	}

	/**
	 * Returns the sudoku field. 
	 * @return array
	 */
	public function getField() {
		return $this->_field;
	}

	/**
	 * get the content of the cell at the given row and column. 
	 */
	public function getCell($row, $column) {
		return $this->_field[$row][$column];
	}

	/**
	 * checks whether inserting a number into a special cell is allowed. 
	 * @param  int $number
	 * @param  int $row
	 * @param  int $column
	 * @return bool
	 */
	public function checkInsert($number, $row, $column) {
		// generally allow the insertion of 0, the emptying of the field. 
		if ($number == 0) {
			return true;
		}

		$block = $this->getNumbersInBlock($row, $column);

		for ($i = 0; $i < 9; $i++) {
			// checks the row for identical numbers
			if ($this->_field[$row][$i] == $number) {
				return false;
			}
			// checks the column for identical numbers
			if ($this->_field[$i][$column] == $number) {
				return false;
			}
			// checks the block for identical numbers
			if ($block[$i] == $number) {
				return false;
			}
		}

		// nothing identical was found. Insert is allowed. 
		return true;
	}

	/**
	 * returns all numbers of the cells in the block in which the given cell is. 
	 * @param  int $row
	 * @param  int $column
	 * @return array
	 */
	public function getNumbersInBlock($row, $column, $includePosition=false) {
		$block = array();

		$topLeftRow		= floor($row / 3) * 3;
		$topLeftColumn	= floor($column / 3) * 3;

		for ($i = 0; $i < 3; $i++) {
			for ($j = 0; $j < 3; $j++) {
				$row 		= $topLeftRow + $i;
				$column 	= $topLeftColumn + $j;
				$block[] 	= $this->_field[$row][$column];
			}
		}

		return $block;	
	}

	public function emptyFieldsIterator() {
		require_once('SudokuFieldIterator.php');
		$iterator = new SudokuFieldIterator($this, SudokuFieldIterator::EMPTY_FIELDS);
		return $iterator;
	}

	public function allFieldsIterator() {
		require_once('SudokuFieldIterator.php');
		$iterator = new SudokuFieldIterator($this, SudokuFieldIterator::ALL_FIELDS);
		return $iterator;
	}

	public function isComplete() {
		for ($i = 0; $i < 9; $i++) {
			for ($j = 0; $j < 9; $j++) {
				if ($this->_field[$i][$j] == 0) return false;
			}
		}
		return true;
	}

	public function countZeroes() {
		$zeroes = 0;
		for ($i = 0; $i < 9; $i++) {
			for ($j = 0; $j < 9; $j++) {
				if ($this->_field[$i][$j] == 0) $zeroes++;
			}
		}
		return $zeroes;
	}

	public function toString() {
		$result = '';
		for ($i = 0; $i < 9; $i++) {
			for ($j = 0; $j < 9; $j++) {
				$result .= $this->_field[$i][$j] . ' ';
				if ($j % 3 == 2) {
					$result .= ' ';
				}
			}
			$result .= "\n";
			if ($i % 3 == 2) {
				$result .= "\n";
			}
		}
		return $result;
	}

}
