<?php

namespace BAProject\SudokuBundle\Sudoku;

class SudokuGenerator {

    const DEBUG = 0;

    /**
     * This function generates the numbers for a complete sudoku field. 
     * you can also insert specific numbers you want to exist in the sudoku. 
     * @param  array $startField [optional] the start field containing numbers that shall be included in the generated sudokku field. 
     * @return SudokuField  The generated sudoku field. 
     */
    public static function generate($startField = null) {
        
        $original = new SudokuField($startField);
        $newField = new SudokuField($startField);
        
        $field = new SudokuField();
        
        $tries = array();
        for ($i = 0; $i < 9; $i++) {
            for ($j = 0; $j < 9; $j++) {
                $tries[$i][$j] = array();
            }
        }

        $iterator = $original->emptyFieldsIterator();
        $iterator->next();
        return self::recursiveGenerate($newField, $iterator, $tries);
    }

    /**
     * The recursive generator function. 
     * This function is called each time we iterate through a new field. 
     * It randomly tries inserting numbers from left to right, top to bottom. If it finds a field in which 
     * nothing can be inserted, it calls itself going back one field and tries inserting another number in 
     * that field. 
     * To speed the process up, it saves all the numbers already having been tried in a field. 
     * Also, for inserting random numbers, it shuffles an array of numbers from 1 to 9. This way we make sure we only try 
     * each number once. 
     * @param  SudokuField $field            The Sudoku Field in which the Sudoku is generated. 
     * @param  SudokuFieldIterator $iterator The SudokuFieldIterator, which allows us to locate the field we are iterating over at the moment. 
     * @param  array $tries                  The array that saves every number tried in every field. 
     * @param  integer $recursiveCalls       Counter for the amount of recursive calls for analytical reasons. 
     * @return SudokuField		             The two-dimensional sudoku field. 
     */ 
    private static function recursiveGenerate($field, $iterator, $tries, $recursiveCalls = 0) {
        // empty the field we are working on, shuffle the numbers we will test in the for loop.
        $row = $iterator->getRow();
        $column = $iterator->getColumn();
        $field->insert(0, $row, $column);
        $numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        shuffle($numbers);

        if (self::DEBUG > 1) echo "recursive call $recursiveCalls on cell ($row,$column)...\nshuffled numbers: " . implode(", ", $numbers) . "\n";
        for ($i = 0; $i < 9; $i++) {
            if (self::DEBUG > 1) echo "\t" . $numbers[$i] . " ";
            
            // continue if number was tried already.
            if (in_array($numbers[$i], $tries[$row][$column])) {
                if (self::DEBUG > 1) echo "was tried already.\n";
                continue;
            }
            // if not, add number to the tried numbers.
            $tries[$row][$column][] = $numbers[$i];
            
            //try to insert the number into the sudoku field. 
            if ($field->insert($numbers[$i], $row, $column)) {
                if (self::DEBUG > 1) echo "could be inserted!\n";
                
                // if this was the last field we are done. 
                if ($iterator->next() === false) {
                    if (self::DEBUG == 1) echo "\n $recursiveCalls recursive calls.\n\n";
                    if (self::DEBUG > 1) echo "done and successful. returning completed sudoku.\n\n" . $field->toString();
                    return $field;
                }
                // if we find another field, we just have moved to the next field (see if clause) 
                // and call this function recursively.
                if (self::DEBUG > 1) echo "\tnext recursive layer...\n\n";
                return self::recursiveGenerate($field, $iterator, $tries, $recursiveCalls + 1);
            }
            // number could not be inserted. 
            if (self::DEBUG > 1) echo " couldn't be inserted.\n";
        }
        
        // no number could be inserted. 
        if (self::DEBUG > 1) echo "\t nothing could be inserted. going back one field...\n";
        
        // reset the tries (they rely on the previous fields and they are about to be changed)
        $tries[$row][$column] = array();
        
        // if we cannot go back anymore, the algorithm failed. 
        if ($iterator->previous() === false) {
            if (self::DEBUG == 1) echo "\n $recursiveCalls recursive calls.\n\n";
            if (self::DEBUG > 1) echo "\ncan't go back anymore... failed!!!\n\n";
            return false;
        }
        if (self::DEBUG > 1) echo "\t next recursive layer...\n\n";
        // standard behaviour: call the function recursively for teh previous field. 
        return self::recursiveGenerate($field, $iterator, $tries, $recursiveCalls + 1);
    }
    
    /**
     * This function removes numbers from a complete sudoku field so that there are $n given numbers left. 
     * The difficulty in this algorithm is, that the sudoku field with the numbers being removed still has to be solveable. First, we remove 3 random numbers from the sudoku field. Next, we remove a random number from each row and each column that is completely filled. After that we try to remove a random number and check if the sudoku field still has one solution for so long that we only have $n given numbers in the sudoku or we cannot remove any numbers anymore. 
     * @param  SudokuField &$field The (complete) sudoku field object from which the numbers shall be removed. 
     * @param  int         $n      The numbers that shall be left in the sudoku field (given numbers)
     * @return int                 The actual given numbers of $field.
     * @see  SudokuGenerator::reomveRandomNumber
     * @see  SudokuGenerator::removeFirstNumbers
     * @see  SudokuGenerator::removeRowsAndColumns
     */
    public static function pluck(&$field, $n) {
        $givenNumbers = 81;
        $givenNumbers -= self::removeFirstNumbers($field);
        $givenNumbers -= self::removeRowsAndColumns($field);
        
        for ($i = 0; $givenNumbers > $n; $i++) {
            if (self::DEBUG > 1) echo "iteration $i\n";
            if (!self::removeRandomNumber($field)) {
                if (self::DEBUG > 0) echo "couldn't remove any further fields.\n\n";
                break;
            }
            $givenNumbers--;
        }

        if (self::DEBUG > 0) echo "$i iterations. \n";
        return $givenNumbers;
    }

    /**
     * Tries to remove a random number from the field and checks whether the sudoku still has only one solution. 
     * If no number could be removed, this function returns false. 
     * @param  SudokuField  $field  The field from which one number shall be removed. 
     * @return boolean              True if number removal is successful, false otherwise. 
     */
    public static function removeRandomNumber($field) {
        $filledCells = array();
        
        for ($i = 0; $i < 9; $i++) {
            for ($j = 0; $j < 9; $j ++) {
                if ($field->getCell($i, $j) != 0) {
                    $filledCells[] = array($i, $j);
                }
            }
        }
        $i = 0;
        while (count($filledCells)) {
            if (self::DEBUG > 1) echo "removeRandomNumber iteration $i\n";
            $i++;
            $index = rand(0, count($filledCells)-1);
            $cell = $filledCells[$index];
            array_splice($filledCells, $index, 1);
            $row = $cell[0];
            $column = $cell[1];

            $copy = SudokuField::copy($field);
            $copy->insert(0, $row, $column);
            if (count(SudokuSolver::solve($copy, 2, self::DEBUG-1)) == 1) {
                $field->insert(0, $row, $column);
                $copy = null;
                return true;
            }
            $copy = null;
        }
        return false;
    }

    public static function removeFirstNumbers($field) {
        for ($i = 0; $i < 3; $i++) {
            $row = rand(0, 8);
            $column = rand(0, 8);
            if ($field->getCell($row, $column) != 0) {
                $field->insert(0, $row, $column);
            } else {
                $i--;
            }
        }
        return 3;
    }

    public static function removeRowsAndColumns($field) {
        $removedNumbers = 0;
        for ($i = 0; $i < 9; $i++) {
            $rowComplete = true;
            for ($j = 0; $j < 9; $j++) {
                if ($field->getCell($i, $j) == 0) $rowComplete = false;
            }
            if ($rowComplete) {
                $field->insert(0, $i, rand(0, 8));
                $removedNumbers++;
            }

            $columnComplete = true;
            for ($j = 0; $j < 9; $j++) {
                if ($field->getCell($j, $i) == 0) $columnComplete = false;
            }
            if ($columnComplete) {
                $field->insert(0, rand(0, 8), $i);
                $removedNumbers++;
            }
        }
        return $removedNumbers;
    }

}
