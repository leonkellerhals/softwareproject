<?php
namespace BAProject\SudokuBundle\Sudoku;

use BAProject\SudokuBundle\Sudoku\Exception\InvalidInsertionException;

class SudokuSolver {

	public static function oldSolve($field, $maxSolutions = 1, $debug = 0) {

		if ($field instanceof SudokuField) {
			$original = SudokuField::copy($field);
		} elseif (is_array($field)) {
			try {
				$original = new SudokuField($field);
				$field = new SudokuField($field);	
			} catch(InvalidInsertionException $e) {
				return false;
			}
		} else {
			return array();
		}

		$iterator = $original->emptyFieldsIterator();
		$lastTry = array();
		while ($iterator->next() !== false) {
			$lastTry[$iterator->getRow()][$iterator->getColumn()] = 0;
		}

		$iterator->reset();
		$iterator->next();
		return self::recursiveSolve($field, $iterator, $lastTry, $debug, $maxSolutions, array());
	}

	private static function recursiveSolve(&$field, &$iterator, &$lastTry, $debug, $maxSolutions, $solutions, $recursiveCalls = 0) {

		$row = $iterator->getRow();
		$column = $iterator->getColumn();

		$field->insert(0, $row, $column);
		if ($debug > 1) echo "recursive call $recursiveCalls on cell ($row,$column)...\n";

		$startValue = $lastTry[$row][$column] + 1;
		if ($debug > 1) echo "start value: $startValue\n";
		for ($i = $startValue; $i <= 9; $i++) {
			if ($debug > 1) echo "\ttesting number $i\n";
			$lastTry[$row][$column] = $i;
			if ($field->insert($i, $row, $column)) {
				if ($debug > 1) echo "\tcould be inserted!\n";
				if ($iterator->next() === false) {
					if ($debug > 0) {
						echo "found solution:\n";
						echo $field->toString();
					}
					$solutions[] = $field->getField();
					if (count($solutions) >= $maxSolutions) {
						if ($debug > 0) echo "reached maximum amount of solutions.\n";
						echo "recursive calls: $recursiveCalls \n";
						return $solutions;
					}
					if ($debug > 1) echo "\tnext recursive layer...\n\n";
					$iterator->previous();
					return self::recursiveSolve($field, $iterator, $lastTry, $debug, $maxSolutions, $solutions, $recursiveCalls + 1);
				}
				if ($debug > 1) echo "\tnext recursive layer...\n\n";
				return self::recursiveSolve($field, $iterator, $lastTry, $debug, $maxSolutions, $solutions, $recursiveCalls + 1);
			}
		}
		if ($debug > 1) echo "\tnothing could be inserted. trying to go back one field...\n";
		$lastTry[$row][$column] = 0;
		if ($iterator->previous() === false) {
			if ($debug > 1) echo "recursive calls: $recursiveCalls \n";
			echo "recursive calls: $recursiveCalls \n";
			if ($debug > 0) echo "can't go back anymore... done!\n\n";
			return $solutions;
		}
		if ($debug > 1) echo "\tnext recursive layer...\n\n";
		return self::recursiveSolve($field, $iterator, $lastTry, $debug, $maxSolutions, $solutions, $recursiveCalls + 1);
	}

	public static function solve($field, $maxSolutions = 1, $debug = 0) {
	
		if ($field instanceof SudokuField) {
			$original = SudokuField::copy($field);
		} elseif (is_array($field)) {
			try {
				$original = new SudokuField($field);
				$field = new SudokuField($field);	
			} catch(InvalidInsertionException $e) {
				return false;
			}
		} else {
			return array();
		}

		$solutions = array();
		if ($maxSolutions == -1) $maxSolutions = PHP_INT_MAX;
		$iterator = $original->emptyFieldsIterator();
		$lastTry = array();
		while ($iterator->next() !== false) {
			$lastTry[$iterator->getRow()][$iterator->getColumn()] = 0;
		}

		$iterator->reset();
		$iterator->next();
		
		$iteration = 0;
		while (count($solutions) < $maxSolutions) {
			if ($debug > 2) echo "iteration $iteration\n";
			$iteration++;
			$row = $iterator->getRow();
			$column = $iterator->getColumn();

			$field->insert(0, $row, $column);
			
			$startValue = $lastTry[$row][$column] + 1;
			$couldBeInserted = false;
			if ($debug > 1) echo "start value: $startValue\n";
			for ($i = $startValue; $i <= 9 && !$couldBeInserted; $i++) {
				if ($debug > 1) echo "\ttesting number $i\n";
				$lastTry[$row][$column] = $i;
				if ($couldBeInserted = $field->insert($i, $row, $column)) {
					if ($debug > 1) echo "\tcould be inserted!\n";
					if ($iterator->next() === false) {
						if ($debug > 0) {
							echo "found solution:\n";
							echo $field->toString();
						}
						$solutions[] = $field->getField();
						$iterator->previous();
					}
				}
			}
			if (!$couldBeInserted) {
				if ($debug > 1) echo "\tnothing could be inserted. trying to go back one field...\n";
				$lastTry[$row][$column] = 0;
				if ($iterator->previous() === false) {
					if ($debug > 0) echo "can't go back anymore... done!\n\n";
					break;
				}
			}
		}
		return $solutions;
	}

}