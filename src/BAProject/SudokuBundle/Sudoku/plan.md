# Generation Plan

Generating a solveable sudoku contains of two steps: 
1. Make a solving algorithm so that you understand a little more about how to deal with sudokus. 
2. Generate a completely filled sudoku
3. Remove numbers of the sudoku so that it stays solveable

## 1. Make a sudoku solver. 
For starters, we are going to create a solver basing on a depth-first search. 


## 2. Generate a sudoku

There are diffreent ways to approach the generation of a sudoku. 

## 3. Remove Numbers

When removing numbers from a sudoku you have to make sure that it still only has one legal solution. Already with four numbers removed from the sudoku it is possible to have two solutions. This scenario needs to be avoided. 

The process of removing numbers contains of two steps. The first step is to remove an initial set of numbers from the sudoku to prepare the second step that is calles rule based cutting. 

The preparation is very quickly dealt with: First, remove three random numbers from the freshly generated sudoku. Second, remove a random number from each row, column and secion that still contains 9 numbers. Once having done these two steps one has removed between 9 and 21 numbers from the complete sudoku. 

The harder part is the second step, in which we will remove numbers from the sudoku based on the rules. The rules are the following: 

1. Set each cell onto 0 if all the own value from 1 to 9 is set in the adjacent cells. Example:

`
 5 | 4 | 9
 --|---|---
 3 |(2)| 7
 --|---|---
 1 | 8 | 6
`

