<?php
namespace BAProject\SudokuBundle\Exception;

class InvalidGameRecordException extends \Exception {

    public static $INVALIDDATA = 'invaliddata';
    public static $NOLASTGAME = 'nolastgame';

    private $reason;

    public function __construct($sReason) {
        $this->reason = $sReason;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

}