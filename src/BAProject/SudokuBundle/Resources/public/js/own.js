function chooseValue(element) {

    /**
     * Clicked Cell
     * @type {*|jQuery|HTMLElement}
     */
    oClickedCell = $(element);

    // unset previous selected buttons
    $('.cellButton.btn-success').each(function() {
        oCurrentCell = $(this);
        aCurrentCellCoordinates = [oCurrentCell.data('row'), oCurrentCell.data('col')];

        currentBlock = getBlockOfField(aCurrentCellCoordinates[0], aCurrentCellCoordinates[1]);
        if(currentBlock %2 == 0) {
            oCurrentCell.addClass('btn-primary');
        } else {
            oCurrentCell.addClass('btn-info');
        }
        $(this).removeClass('btn-success');


    })

    /**
     *
     * @type {*[ROW, COLUMN]}
     */
    aCellCoordinates = [oClickedCell.data('row'), oClickedCell.data('col')];

    oClickedCell.addClass('btn-success');
    currentBlock = getBlockOfField(aCellCoordinates[0], aCellCoordinates[1]);
    if(currentBlock %2 == 0) {
        oClickedCell.removeClass('btn-primary');
    } else {
        oClickedCell.removeClass('btn-info');
    }

    gameFillerWrapper = $('#gameFillerWrapper');
    gameFillerWrapper.data('selectedrow',parseInt(aCellCoordinates[0]));
    gameFillerWrapper.data('selectedcol',parseInt(aCellCoordinates[1]));
    gameFillerWrapper.show().removeClass('hide');
}

function setNumber(clickedButton) {

    oClickedButton = $(clickedButton);

    iChoosenNumber = oClickedButton.html();

    gameFillerWrapper = $('#gameFillerWrapper');

    aCellCoordinates = [gameFillerWrapper.data('selectedrow'), gameFillerWrapper.data('selectedcol')];

    oTargetFieldCell = $('#gameFieldWrapper').find('[data-row='+aCellCoordinates[0]+'][data-col='+aCellCoordinates[1]+']');
    oTargetFieldCell.val(iChoosenNumber);
    oTargetFieldCell.removeClass('btn-success');

    aTargetCellCoordinates = [oTargetFieldCell.data('row'), oTargetFieldCell.data('col')];
    currentBlock = getBlockOfField(aTargetCellCoordinates[0], aTargetCellCoordinates[1]);
    if(currentBlock %2 == 0) {
        oTargetFieldCell.addClass('btn-primary');
    } else {
        oTargetFieldCell.addClass('btn-info');
    }

    gameFillerWrapper.data('selectedrow',0);
    gameFillerWrapper.data('selectedcol',0);
    gameFillerWrapper.hide();

    /**
     * Hidden input field for gameFieldDataArray
     * @type {*|jQuery|HTMLElement}
     */
    gameFieldDataInput = $('#aInserted');
    aGameFieldData = $.parseJSON(gameFieldDataInput.val());
    aGameFieldData[aCellCoordinates[0]][aCellCoordinates[1]] = parseInt(iChoosenNumber);
    sGameFieldData = JSON.stringify(aGameFieldData);
    gameFieldDataInput.val(sGameFieldData);
    saveInsertedGamefield(iChoosenNumber, aCellCoordinates[0], aCellCoordinates[1]);
}

function saveInsertedGamefield (sNumber, sRow, sCol) {
    sDataString = JSON.stringify([sNumber, sRow, sCol]);
    sUrl = $('#gameFieldWrapper').data('savepath');
    $.ajax({
        url: sUrl+sDataString,
        success: function(result) {
            return true;
        },
        error: function (result) {
            return false;
        }
    });
}

function getBlockOfField(row, col) {
    mainRow = Math.floor(row / 3) * 3;
    mainCol = Math.floor(col / 3) * 3;

    block = (mainCol+mainRow) - 1;

    return block;
}

$(document).ready(function() {

    /* Show register form on landing page and hide login panel if there was an error */
    if($('#fos_user_registration_form>div>ul').get(0) != undefined) {
        $('#loginPanel').collapse('hide');
        $('#registerPanel').collapse('show');
    }
});