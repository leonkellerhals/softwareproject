<?php

require_once('../SudokuField.php');

class SudokuFieldCounter extends PHPUnit_Framework_TestCase {

	public function testGetRow() {
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);	

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		for ($i = 0; $i < 11; $i++) {
			$iterator->next();
		}

		$this->assertEquals(1, $iterator->getRow());
	}

	public function testGetColumn() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);	

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		for ($i = 0; $i < 12; $i++) {
			$iterator->next();
		}

		$this->assertEquals(2, $iterator->getColumn());
	}

	public function testGetElement() {
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);	

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		$this->assertNull($iterator->getElement);
		for ($i = 0; $i < 13; $i++) {
			$iterator->next();
		}

		$this->assertEquals(1, $iterator->getElement());
	}

	public function testReset() {
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);	

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		$iterator->next();
		$iterator->reset();

		$this->assertNull($iterator->getElement());
	}

	public function testNextWithAllFields() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		$this->assertEquals(5, $iterator->next());
		
		for ($i = 0; $i < 11; $i++) {
			$iterator->next();
		}
		$this->assertEquals(1, $iterator->next());
	}

	public function testPreviouswithAllFields() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		$i = 0;
		while ($iterator->next() !== false) {
			$i++;
			if ($i > 81) {
				$this->fail('iterated more often than a sudoku field has cells. aborting.');
			}
		}
		$this->assertEquals(7, $iterator->previous());
	}

	public function testNextWithEmptyFields() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->emptyFieldsIterator();

		$i = 0;
		while ($iterator->next() !== false) {
			$i++;
			if ($i > 81) {
				$this->fail('iterated more often than a sudoku field has cells. aborting.');
			}
		}
		$this->assertEquals(51, $i);
	}

	public function testPreviousWithEmptyFields() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->emptyFieldsIterator();

		for ($i = 0; $i < 2; $i++) {
			$iterator->next();
		}
		$this->assertEquals(0, $iterator->previous());
		$this->assertEquals(2, $iterator->getColumn());
	}
}