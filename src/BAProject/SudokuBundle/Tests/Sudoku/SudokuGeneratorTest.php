<?php 

namespace BAProject\SudokuBundle\Tests\Sudoku;

use BAProject\SudokuBundle\Sudoku\SudokuGenerator;

class SudokuGeneratorTest extends \PHPUnit_Framework_TestCase {

	public function testGenerate() {

		$sudoku = SudokuGenerator::generate();
		$this->assertInstanceOf('SudokuField', $sudoku);
		$this->assertTrue($sudoku->isComplete());
		echo "\n\n" . $sudoku->toString();
	}

	public function testGenerateWithGivenNumbers() {

		$field = array(
			array(9, 0, 2,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 5, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 9,  0, 0, 0,  0, 0, 0)
		);

		$sudoku = SudokuGenerator::generate($field);
		$this->assertInstanceOf('SudokuField', $sudoku);
		$this->assertTrue($sudoku->isComplete());

		echo "\n\n" . $sudoku->toString();
	}

	/**
	 * @expectedException InvalidInsertionException
	 */
	public function testGenerateWithWrongNumbers() {
		$field = array(
			array(9, 0, 2,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 2, 0,  0, 0, 0,  0, 0, 0), 
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 5, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 0,  0, 0, 0,  0, 0, 0), 
			array(0, 0, 9,  0, 0, 0,  0, 0, 0)
		);

		$sudoku = SudokuGenerator::generate($field);
	}

	public function testRemoveNumbers() {
		$field = array(
			array(5, 3, 4,  6, 7, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);

		$full = new SudokuField($field);
		$empty = SudokuGenerator::removeNumbers($full);
		$this->assertFalse($empty->isComplete());

		echo "\n\n" . $empty->toString() . "\n\n";
	}

	/**
	 * @expectedException SudokuNotCompleteException
	 */
	public function testRemoveNumbersFailure() {
		$field = array(
			array(5, 3, 4,  6, 0, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);

		$notFull = new SudokuField($field);
		SudokuGenerator::removeNumberS($notFull);
	}
}