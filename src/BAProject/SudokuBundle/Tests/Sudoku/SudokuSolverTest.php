<?php

namespace BAProject\SudokuBundle\Tests\Sudoku;

use BAProject\SudokuBundle\Sudoku\SudokuSolver;

class SudokuSolverTest extends \PHPUnit_Framework_TestCase {

	public function testSolve() {

		$unsolvedField = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$actualSolvedField = SudokuSolver::solve($unsolvedField, -1);

		$assertedSolvedField = array(
			array(5, 3, 4,  6, 7, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);

		$this->assertEquals($actualSolvedField[0], $assertedSolvedField);
	}

	public function testSolveWithBadInput() {

		$unsolvedField = array(
			array(5, 3, 0,  0, 7, 0,  2, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$this->assertFalse(SudokuSolver::solve($unsolvedField));
	}

	public function testSolveWithMultipleSolutions() {
		$unsolvedField = array(
			array(0, 0, 3,  4, 5, 6,  7, 8, 9), 
			array(4, 5, 6,  7, 8, 9,  1, 2, 3),
			array(7, 8, 9,  1, 2, 3,  4, 5, 6), 

			array(0, 0, 4,  3, 9, 8,  5, 6, 7), 
			array(8, 6, 5,  2, 7, 1,  3, 9, 4), 
			array(9, 3, 7,  6, 4, 5,  8, 1, 2), 

			array(3, 4, 1,  8, 6, 2,  9, 7, 5), 
			array(5, 7, 2,  9, 1, 4,  6, 3, 8), 
			array(6, 9, 8,  5, 3, 7,  2, 4, 1)
		);

		$assertedSolvedField = array(
			array(
				array(1, 2, 3,  4, 5, 6,  7, 8, 9), 
				array(4, 5, 6,  7, 8, 9,  1, 2, 3),
				array(7, 8, 9,  1, 2, 3,  4, 5, 6), 

				array(2, 1, 4,  3, 9, 8,  5, 6, 7), 
				array(8, 6, 5,  2, 7, 1,  3, 9, 4), 
				array(9, 3, 7,  6, 4, 5,  8, 1, 2), 

				array(3, 4, 1,  8, 6, 2,  9, 7, 5), 
				array(5, 7, 2,  9, 1, 4,  6, 3, 8), 
				array(6, 9, 8,  5, 3, 7,  2, 4, 1)
			),
			array(
				array(2, 1, 3,  4, 5, 6,  7, 8, 9), 
				array(4, 5, 6,  7, 8, 9,  1, 2, 3),
				array(7, 8, 9,  1, 2, 3,  4, 5, 6), 

				array(1, 2, 4,  3, 9, 8,  5, 6, 7), 
				array(8, 6, 5,  2, 7, 1,  3, 9, 4), 
				array(9, 3, 7,  6, 4, 5,  8, 1, 2), 

				array(3, 4, 1,  8, 6, 2,  9, 7, 5), 
				array(5, 7, 2,  9, 1, 4,  6, 3, 8), 
				array(6, 9, 8,  5, 3, 7,  2, 4, 1)
			),
		);

		$actualSolvedField = SudokuSolver::solve($unsolvedField, -1);
		
		$this->assertEquals($assertedSolvedField, $actualSolvedField);
	}
}