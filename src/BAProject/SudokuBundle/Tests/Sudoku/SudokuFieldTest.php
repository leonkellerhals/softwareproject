<?php

require_once('../SudokuField.php');

class SudokuFieldTest extends PHPUnit_Framework_TestCase {
	public function testField() {
		
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);
		$sf = new SudokuField($field);

		$assertedField = $sf->getField();

		$this->assertEquals($field, $assertedField);
	}

	/**
	 * @expectedException InvalidInsertionException
	 */
	public function testFieldException() {
		$field = array(
			array(5, 3, 7,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
	}

	public function testCheckInsert() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);

		$this->assertTrue($sf->insert(8, 5, 6), 'legal insert');
		
		$this->assertFalse($sf->insert(7, 5, 6), 'illegal insert, same number in row');
		$this->assertFalse($sf->insert(5, 0, 8), 'illegal insert, same number in column');
		$this->assertFalse($sf->insert(3, 5, 6), 'illegal insert, same number in block');
	}

	public function testInsert() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);

		$insertedNumber = 8;
		$this->assertTrue($sf->insert($insertedNumber, 5, 6), 'inserting');
		
		$actualNumber = $sf->getCell(5, 6);
		$this->assertEquals($insertedNumber, $actualNumber, 'testing insert');
	}

	public function testGetCell() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);

		$this->assertEquals(8, $sf->getCell(2, 2));
	}

	public function testGetNumbersInBlock() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);

		$assertedBlock = array(0, 6, 0, 8, 0, 3, 0, 2, 0);

		$this->assertEquals($sf->getNumbersInBlock(3, 3), $assertedBlock, 'test 1');
		$this->assertEquals($sf->getNumbersInBlock(4, 5), $assertedBlock, 'test 1');
	}

	public function testEmptyFieldsIterator() {
		
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->emptyFieldsIterator();

		$this->assertEquals($iterator->next(), 0);
	}

	public function testAllFieldsIterator() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$iterator = $sf->allFieldsIterator();

		$this->assertEquals($iterator->next(), 5);
	}

	public function testIsComplete() {
		$field = array(
			array(5, 3, 4,  6, 7, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);

		$sf = new SudokuField($field);
		$this->assertTrue($sf->isComplete());
	}

	public function testIsCompleteFalse() {
		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);
		$this->assertFalse($sf->isComplete());
	}

	public function testToString() {

		$field = array(
			array(5, 3, 0,  0, 7, 0,  0, 0, 0), 
			array(6, 0, 0,  1, 9, 5,  0, 0, 0), 
			array(0, 9, 8,  0, 0, 0,  0, 6, 0), 

			array(8, 0, 0,  0, 6, 0,  0, 0, 3), 
			array(4, 0, 0,  8, 0, 3,  0, 0, 1), 
			array(7, 0, 0,  0, 2, 0,  0, 0, 6), 

			array(0, 6, 0,  0, 0, 0,  2, 8, 0), 
			array(0, 0, 0,  4, 1, 9,  0, 0, 5), 
			array(0, 0, 0,  0, 8, 0,  0, 7, 9)
		);

		$sf = new SudokuField($field);

		$actualString =  $sf->toString();

		$assertedString = '5 3 0  0 7 0  0 0 0  
6 0 0  1 9 5  0 0 0  
0 9 8  0 0 0  0 6 0  

8 0 0  0 6 0  0 0 3  
4 0 0  8 0 3  0 0 1  
7 0 0  0 2 0  0 0 6  

0 6 0  0 0 0  2 8 0  
0 0 0  4 1 9  0 0 5  
0 0 0  0 8 0  0 7 9  

';

		$this->assertEquals($assertedString, $actualString);
	}

}