<?php 

namespace BAProject\SudokuBundle\Tests\Sudoku;

use BAProject\SudokuBundle\Sudoku\SudokuGenerator;
use BAProject\SudokuBundle\Sudoku\SudokuField;
use BAProject\SudokuBundle\Sudoku\SudokuSolver;

class GeneralSudokuTest extends \PHPUnit_Framework_TestCase {

	public function testRemoveFirstNumbers() {
		$field = array(
			array(5, 3, 4,  6, 7, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);
		$sf = new SudokuField($field);

		SudokuGenerator::removeFirstNumbers($sf);
		$this->assertEquals(1, count(SudokuSolver::solve($sf, 2)));
	}

	public function testRemoveRowsAndColumns() {
		$field = array(
			array(5, 0, 4,  6, 7, 8,  9, 0, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 0, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);
		$sf = new SudokuField($field);

		SudokuGenerator::removeRowsAndColumns($sf);
		$this->assertEquals(1, count(SudokuSolver::solve($sf, 2)));
	}

	public function testPluck() {
		$field = array(
			array(5, 3, 4,  6, 7, 8,  9, 1, 2), 
			array(6, 7, 2,  1, 9, 5,  3, 4, 8), 
			array(1, 9, 8,  3, 4, 2,  5, 6, 7), 

			array(8, 5, 9,  7, 6, 1,  4, 2, 3), 
			array(4, 2, 6,  8, 5, 3,  7, 9, 1), 
			array(7, 1, 3,  9, 2, 4,  8, 5, 6), 

			array(9, 6, 1,  5, 3, 7,  2, 8, 4), 
			array(2, 8, 7,  4, 1, 9,  6, 3, 5), 
			array(3, 4, 5,  2, 8, 6,  1, 7, 9)
		);
		$sf = new SudokuField($field);

		$givenNumbers = SudokuGenerator::pluck($sf, 40);
		$this->assertFalse($givenNumbers === false);
		echo "\n$givenNumbers\n";
		echo $sf->toString();
	}
}