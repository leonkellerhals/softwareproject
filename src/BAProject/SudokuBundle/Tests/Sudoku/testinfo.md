# PHPUnit Tests

In this directory you will find test written in PHPUnit for all sudoku classes and algorithms. 

We have used Sebastian Bergmann's PHPUnit Test Suite for generating those tests. For more info about it, see phpunit.de

These (minimal) steps are required for running the tests: 

1. Download [https://phar.phpunit.de/phpunit.phar]https://phar.phpunit.de/phpunit.phar
2. Run php path/to/phpunit.phar path/to/test.php

If you want to know more about the tests, I recommend the [PHPUnit documentation](https://phpunit.de/manual/4.3/en/index.html).
