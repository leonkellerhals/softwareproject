<?php

namespace BAProject\SudokuBundle\Controller;

use BAProject\SudokuBundle\Entity\Game;
use BAProject\SudokuBundle\Entity\User;
use BAProject\SudokuBundle\Exception\InvalidGameRecordException;
use BAProject\SudokuBundle\Sudoku\Sudoku;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class GameController
 *
 * This controller handels all actions depending on a sudoku game
 *
 * @package BAProject\SudokuBundle\Controller
 */
class GameController extends Controller
{

    /**
     * This action handles data for scoreboard
     *
     * @Template()
     * @return array
     */
    public function scoreAction($message) {

        $aReturn = array();

        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        $oScoredGames = null;

        if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
            $oGameRepository = $this
                ->getDoctrine()
                ->getRepository('BAProjectSudokuBundle:Game');
            $oScoredGames = $oGameRepository->getScoreableGames($oUser->getId());

            $aResults = array();

            foreach($oScoredGames as $oGame /** @var Game $oGame */) {
                
                $iGameTime = $oGame->getGameend()->diff($oGame->getGamestart());
                
                $aResults[$oGame->getDiff()][] = array(
                    'mins' => $iGameTime->format('%I'),
                    'secs' => $iGameTime->format('%S'),
                    'date' => array(
                        'day' => $oGame->getGamestart()->format('d'),
                        'month' => $oGame->getGamestart()->format('m'),
                        'year' => $oGame->getGamestart()->format('Y'),
                    ),
                );
            }

            $aReturn = array(
                'aResults' => $aResults,
                'message' => $message,
            );
        }

        return $aReturn;
    }

    /**
     * This action generates a new game
     *
     * @return array
     */
    public function newAction($diff) {

        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var Session $oSession */
        $oSession = $this->getRequest()->getSession();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        switch ($diff) {
            case 1:
                $sDiff = Sudoku::DIFFICULTY_EASY;
                break;
            case 2:
                $sDiff = Sudoku::DIFFICULTY_MEDIUM;
                break;
            case 3:
                $sDiff = Sudoku::DIFFICULTY_HARD;
                break;
            default:
                $sDiff = Sudoku::DIFFICULTY_EASY;
                break;
        }

        /* Generate new Sudoku and save it in session */
        $oSudoku = Sudoku::newGame($sDiff);
        $oSession->set('oSudoku', $oSudoku);

        /* If the user is logged in, the game is saved to database too */
        if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
            /* Unset lastGame-State for all other games from this user */
            $oGameRepository = $this
                ->getDoctrine()
                ->getRepository('BAProjectSudokuBundle:Game');
            $oGameRepository->unsetLastGameForAllByUser($oUser->getId());

            $oSudoku = Sudoku::newGame();
            $oSession->set('oSudoku', $oSudoku);

            /* A new game will be created and be stored in the database */
            $oGame = new Game();
            $oGame->setGamestart(new \DateTime('now'))
                ->setUser($oUser)
                ->setGamedata($oSudoku->getJSON())
                // TODO: set diff to choosen diff
                ->setDiff(2)
                ->setLastgame(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($oGame);
            $em->flush();
            $oSession->set('lastGameID', $oGame->getId());

        }

        return new RedirectResponse($this->generateUrl('game', array(
            '_locale' => $this->getRequest()->getLocale(),
        )));

    }

    /**
     * This action loads a game from database for logged in users
     *
     * @return Response
     */
    public function loadAction() {

        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var Session $oSession */
        $oSession = $this->getRequest()->getSession();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        // If the user is logged in
        if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
            /* Try to load game from database if there is one */
            try {
                $oGameRepository = $this
                    ->getDoctrine()
                    ->getRepository('BAProjectSudokuBundle:Game');
                /** @var Game $oGame */
                $oGame = $oGameRepository
                    ->findLastGameForUser($oUser->getId());
                $oSudoku = Sudoku::loadJSON($oGame->getGamedata());

                $oGame->setLoaded(true);

                $oSession->set('oSudoku', $oSudoku);
                $em = $this->getDoctrine()->getManager();
                $em->persist($oGame);
                $em->flush();
                $oSession->set('lastGameID', $oGame->getId());

                /* If there is no game in database or an error is detected generate new game */
            } catch (InvalidGameRecordException $e) {

                // If something went wrong with the database data will be reset for continuing
                if ($e->getReason() === InvalidGameRecordException::$INVALIDDATA) {
                    $oGameRepository->unsetLastGameForAllByUser($oUser->getId());
                }
                return new RedirectResponse($this->generateUrl('menu', array(
                    '_locale' => $this->getRequest()->getLocale(),
                    'loadingError' => 'error',
                )));
            }

        }

        return new RedirectResponse($this->generateUrl('game', array(
            '_locale' => $this->getRequest()->getLocale(),
        )));
    }

    /**
     * This action handles data to display a game
     *
     * @Template()
     * @return array
     */
    public function gameAction($message) {
        $oSession = $this->getRequest()->getSession();

        /** @var Sudoku $oSudoku */
        $oSudoku = $oSession->get('oSudoku');

        return array(
            'aInserted' => $oSudoku->getInserted(),
            'aOriginal' => $oSudoku->getOriginal(),
            'message' => $message,
        );
    }

    /**
     * This action saves a game to session and database
     *
     * @return Response
     */
    public function saveAction($sDataString) {
        $aDataString = json_decode($sDataString);

        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var Session $oSession */
        $oSession = $this->getRequest()->getSession();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        /** @var Sudoku $oSudoku */
        $oSudoku = $oSession->get('oSudoku');

        $oSudoku->insert(
            $aDataString[0],
            $aDataString[1],
            $aDataString[2]
        );

        // If the user is logged in
        if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
            try {
                $oGameRepository = $this
                    ->getDoctrine()
                    ->getRepository('BAProjectSudokuBundle:Game');
                /** @var Game $oGame */
                $oGame = $oGameRepository
                    ->findLastGameForUser($oUser->getId());
                $oGame->setGamedata($oSudoku->getJSON());
                $em = $this->getDoctrine()->getManager();
                $em->persist($oGame);
                $em->flush();
            } catch (\Exception $e) {
                return new Response('error');
            }
        }

        $oSession->set('oSudoku', $oSudoku);


        return new Response('success');
    }

    /**
     * This actions is used to display difficulty chooser
     *
     * @Template()
     * @return array
     */
    public function chooseDifficultyAction() {
        return array();
    }

    /**
     * This action checks, if the sudoku is correctly solved
     *
     * @return RedirectResponse
     */
    public function checkAction() {

        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var Session $oSession */
        $oSession = $this->getRequest()->getSession();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        /** @var Sudoku $oSudoku */
        $oSudoku = $oSession->get('oSudoku');
        $sGameID = $oSession->get('lastGameID');

        $bIsSudokuValid = $oSudoku->check();
        if($bIsSudokuValid === true) {
            $sCheckMessage = 'success';

            // If the user is logged in
            if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
                try {
                    $oGameRepository = $this
                        ->getDoctrine()
                        ->getRepository('BAProjectSudokuBundle:Game');
                    /** @var Game $oGame */
                    $oGame = $oGameRepository
                        ->findGameBySudokuAndUser($oUser->getId(), $oSudoku->getJSON(), $sGameID);

                    $oGame->setGameend(new \DateTime('now'));
                    $oGame->setLastgame(false);

                    if($oGame->getLoaded() === true) {
                        $oGame->setScored(false);
                        $sCheckMessage = 'loaded';
                        $oRedirectResponse = new RedirectResponse(
                            $this->generateUrl('game',
                                array(
                                    '_locale' => $this->getRequest()->getLocale(),
                                    'message' => $sCheckMessage
                                )
                            ));
                    } else {
                        $oGame->setScored(true);
                        $oRedirectResponse = new RedirectResponse(
                            $this->generateUrl('score',
                                array(
                                    '_locale' => $this->getRequest()->getLocale(),
                                    'message' => $sCheckMessage,
                                )
                            ));
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($oGame);
                    $em->flush();

                } catch (\Exception $e) {
                    return new RedirectResponse(
                        $this->generateUrl('error',
                            array(
                                '_locale' => $this->getRequest()->getLocale(),
                            )
                        ));
                }
            } else {
                $oRedirectResponse = new RedirectResponse($this->generateUrl(
                    'game',
                    array(
                        '_locale' => $this->getRequest()->getLocale(),
                        'message' => $sCheckMessage,
                    )
                ));

            }
        } else {
            $sCheckMessage = 'error';
            $oRedirectResponse = new RedirectResponse($this->generateUrl(
                'game',
                array(
                    '_locale' => $this->getRequest()->getLocale(),
                    'message' => $sCheckMessage,
                )
            ));
        }
        return $oRedirectResponse;
    }
}
