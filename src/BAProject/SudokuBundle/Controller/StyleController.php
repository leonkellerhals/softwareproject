<?php
namespace BAProject\SudokuBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class StyleController
 *
 * This controller handles the twig-generated CSS file
 *
 * @package BAProject\SudokuBundle\Controller
 */
class StyleController extends Controller {

    /**
     * This action is used for default.twig.css
     *
     * @Template()
     * @return array
     */
    public function defaultAction() {
        return array('h1color' => '#428bca');
    } // defaultAction
} // class