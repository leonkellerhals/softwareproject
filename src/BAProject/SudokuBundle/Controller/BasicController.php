<?php

namespace BAProject\SudokuBundle\Controller;

use BAProject\SudokuBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class BasicController
 *
 * This controller handles all actions depending on basic website functionalities
 *
 * @package BAProject\SudokuBundle\Controller
 */
class BasicController extends Controller
{
    /**
     * This actions handles the game menu frontend data
     *
     * @Template()
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function menuAction($loadingError)
    {
        /** @var User $oUser */
        $oUser = $this->getUser();

        /** @var SecurityContext $oSecurityContext */
        $oSecurityContext = $this->get('security.context');

        $bUserHasLastGame = false;

        if($oUser !== null && $oSecurityContext->isGranted('ROLE_USER')) {
            $oGameRepository = $this
                ->getDoctrine()
                ->getRepository('BAProjectSudokuBundle:Game');
            $bUserHasLastGame = $oGameRepository->hasUserLastGame($oUser->getId());
        }
        return array(
            'bUserHasLastGame' => $bUserHasLastGame,
        );
    }

    /**
     * This action is used for the landing page
     *
     * @Template()
     * @return array
     */
    public function landingAction() {
        return array();
    }

    /**
     * This action is called, if an program error is occurred
     *
     * @Template()
     * @return array
     */
    public function errorAction() {
        return array();
    }
}
